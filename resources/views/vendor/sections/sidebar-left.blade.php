 <!-- Overlays -->
 <div class="ms-aside-overlay ms-overlay-left ms-toggler" data-target="#ms-side-nav" data-toggle="slideLeft"></div>
 <div class="ms-aside-overlay ms-overlay-right ms-toggler" data-target="#ms-recent-activity" data-toggle="slideRight"></div>

 <!-- Sidebar Navigation Left -->
 <aside id="ms-side-nav" class="side-nav fixed ms-aside-scrollable ms-aside-left">

   <!-- Logo -->
   <div class="logo-sn ms-d-block-lg">
     {{-- <a class="pl-0 ml-0 text-center" href="index.html"> <img src="https://via.placeholder.com/216x62" alt="logo"> </a> --}}
     <a class="pl-0 ml-0 text-center" href="{{ route('admin.dashboard') }}"><h3 class="ms-text-primary">Greendash </h3></a>
   </div>

   <!-- Navigation -->
   <ul class="accordion ms-main-aside fs-14" id="side-nav-accordion">
     <!-- Dashboard -->
     <li class="menu-item">
       <a href="{{ route('admin.dashboard') }}" >
         <span><i class="material-icons fs-16">dashboard</i>Dashboard </span>
       </a>
     </li>
     <!-- Dashboard -->

     <!--- User Management -->
     <li class="menu-item">
       <a href="#" class="has-chevron" data-toggle="collapse" data-target="#user-management" aria-expanded="false" aria-controls="user-management">
         <span><i class="fas fa-users"></i>User Management</span>
       </a>
       <ul id="user-management" class="collapse" aria-labelledby="user-management" data-parent="#side-nav-accordion">
         <li> <a href="{{ route('admin.vendors.index') }}">Vendors</a> </li>
         <li> <a href="{{ route('admin.customers.index') }}">Customers</a> </li>
       </ul>
     </li>
     <!-- User Management -->

     <!--- User Management -->
     <li class="menu-item">
        <a href="#" class="has-chevron" data-toggle="collapse" data-target="#settings" aria-expanded="false" aria-controls="settings">
          <span><i class="flaticon-gear"></i>Settings</span>
        </a>
        <ul id="settings" class="collapse" aria-labelledby="settings" data-parent="#side-nav-accordion">
          <li> <a href="{{route('admin.my-account.edit', Auth::guard('admin')->id())}}">My Account</a> </li>
          <li> <a href="{{route('admin.password.form')}}">Change Password</a> </li>
        </ul>
      </li>
      <!-- User Management -->
   </ul>


 </aside>
