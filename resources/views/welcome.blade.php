@extends('layouts.app')

@section('content')
<div class="container">

</div>
<main>
    <div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
      <div class="col-md-5 p-lg-5 mx-auto my-5">
        <h1 class="display-4 fw-normal">BANNER</h1>

        <a class="btn btn-outline-secondary" href="#">Coming soon</a>
      </div>
      <div class="product-device shadow-sm d-none d-md-block"></div>
      <div class="product-device product-device-2 shadow-sm d-none d-md-block"></div>
    </div>

    <div class="p-5 mb-4 bg-light rounded-3">
        <div class="container-fluid py-5">
          <h1 class="display-5 fw-bold">Lorem Ipsum Doler</h1>
          <p class="col-md-8">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
          <a class="btn btn-primary btn-lg" href="{{ route('register') }}">Sign Up for Free</a>
          <a href="{{ route('login') }}" class="btn btn-outline-primary btn-lg">Login</a>
        </div>
      </div>

  </main>
@endsection
