    <!-- Navigation Bar -->
    <nav class="navbar ms-navbar">

        <div class="ms-aside-toggler ms-toggler pl-0" data-target="#ms-side-nav" data-toggle="slideLeft">
          <span class="ms-toggler-bar bg-primary"></span>
          <span class="ms-toggler-bar bg-primary"></span>
          <span class="ms-toggler-bar bg-primary"></span>
        </div>

        <div class="logo-sn logo-sm ms-d-block-sm">
          <a class="pl-0 ml-0 text-center navbar-brand mr-0" href="index.html"><h3 class="ms-text-primary">ADMIN </h3> </a>
        </div>

        <ul class="ms-nav-list ms-inline mb-0" id="ms-nav-options">


          <li class="ms-nav-item ms-nav-user dropdown">
            <a href="#" id="userDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img class="ms-user-img ms-img-round float-right" src="{{ asset('assets/img/avatar.png') }}" alt="people"> </a>
            <ul class="dropdown-menu dropdown-menu-right user-dropdown" aria-labelledby="userDropdown">
              <li class="dropdown-menu-header">
                <h6 class="dropdown-header ms-inline m-0"><span class="text-disabled">Welcome {{Auth::guard('admin')->user()->firstname}} {{Auth::guard('admin')->user()->lastname}}</span></h6>
              </li>
              <li class="dropdown-divider"></li>
              <li class="ms-dropdown-list">
                <a class="media fs-14 p-2" href="{{route('admin.my-account.edit', Auth::guard('admin')->id())}}"> <span><i class="flaticon-gear mr-2"></i> My Account</span> </a>
              </li>
              <li class="dropdown-menu-footer">
                <a class="media fs-14 p-2" href="{{route('admin.password.form')}}"> <span><i class="flaticon-security mr-2"></i> Change Password</span> </a>
              </li>
              <li class="dropdown-divider"></li>

              <li class="dropdown-menu-footer">
                <a class="media fs-14 p-2" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> <span><i class="flaticon-shut-down mr-2"></i> Logout</span> </a>
                <form id="logout-form" action="{{ 'App\Models\Admin' == Auth::getProvider()->getModel() ? route('admin.logout') : route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
              </li>
            </ul>
          </li>
        </ul>

        <div class="ms-toggler ms-d-block-sm pr-0 ms-nav-toggler" data-toggle="slideDown" data-target="#ms-nav-options">
          <span class="ms-toggler-bar bg-primary"></span>
          <span class="ms-toggler-bar bg-primary"></span>
          <span class="ms-toggler-bar bg-primary"></span>
        </div>

      </nav>
