@extends('layouts.admin')
@section('title','Dashboard')
@section('content')
<div class="row">

    <div class="col-xl-3 col-md-6">
        <a href="{{ route('admin.customers.index') }}">
            <div class="ms-card card-gradient-success ms-widget ms-infographics-widget">
                <div class="ms-card-body media">
                    <div class="media-body">
                        <h6>Customers</h6>
                        <p class="ms-card-change"> {{ $total_customers }}</p>
                    </div>
                </div>
                <i class="fas fa-users"></i>
            </div>
        </a>
    </div>

    <div class="col-xl-3 col-md-6">
        <a href="{{route('admin.my-account.edit', Auth::guard('admin')->id())}}">
            <div class="ms-card card-gradient-secondary ms-widget ms-infographics-widget" style="height: 103px;">
                <div class="ms-card-body media">
                    <div class="media-body">
                        <h6>My Account</h6>
                        <p class="ms-card-change"> </p>
                    </div>
                </div>
                <i class="flaticon-supermarket"></i>
            </div>
        </a>
    </div>

    <div class="col-xl-3 col-md-6">
        <a href="{{route('admin.password.form')}}">
            <div class="ms-card card-gradient-warning ms-widget ms-infographics-widget" style="height: 103px;">
                <div class="ms-card-body media">
                    <div class="media-body">
                        <h6>Change Password</h6>
                        <p class="ms-card-change"></p>
                        <p class="fs-12"></p>
                    </div>
                </div>
                <i class="flaticon-reuse"></i>
            </div>
        </a>
    </div>

    <div class="col-xl-3 col-md-6">
        <a href="{{ route('logout') }}"
            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <div class="ms-card card-gradient-info ms-widget ms-infographics-widget" style="height: 103px;">
                <div class="ms-card-body pos media">
                    <div class="media-body">
                        <h6>Logout</h6>
                        <p class="ms-card-change"></p>
                        <p class="fs-12"></p>
                    </div>
                </div>
                <i class="fas fa-cannabis"></i>
            </div>
        </a>
    </div>

    <div class="col-md-12">
        <div class="ms-panel">
            <div class="ms-panel-header">
                <div class="row">
                    <div class="col-sm-12">
                        <h6>Recent Customers</h6>
                    </div>
                </div>
            </div>
            <div class="ms-panel-body">
                <div class="row">
                    <div class="col-md-12">
                        @if(count($customers) > 0)
                        <div class="table-responsive">
                            <table class="table table-hover thead-dark">
                                <thead>
                                    <tr>
                                        <th scope="col">Customer</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Phone</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($customers as $customer)
                                    <tr>
                                        <td>{{ $customer->firstname }} {{ $customer->lastname }}</td>
                                        <td>{{ $customer->email }}</td>
                                        <td>{{ $customer->phone }}</td>
                                        @if($customer->status == 'inactive')
                                        <td><span class="badge badge-outline-danger">{{ ucfirst($customer->status) }}</span>
                                        </td>
                                        @else
                                        <td><span class="badge badge-outline-light">{{ ucfirst($customer->status) }}</span>
                                        </td>
                                        @endif
                                        <td>
                                            <a href="{{ route('admin.customers.edit', $customer->id) }}"><i class="fas fa-pencil-alt ms-text-success"></i> </a>
                                            <a href="#modal-delete" data-id="{{ $customer->id }}" class="confirmDelete"
                                                data-toggle="modal" data-target="#modal-delete"><i
                                                    class="far fa-trash-alt ms-text-danger"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center">
                        <a href="{{ route('admin.customers.index') }}" class="btn btn-warning btn-sm">View all</a>
                        </div>
                        @else
                        <div class="table-responsive">
                            <p class="text-center">No Listing found.</p>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
