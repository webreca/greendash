<?php

namespace App\Http\Controllers\Vendor\Auth;

use App\Http\Controllers\Controller;
use App\Models\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Validator;

class ChangePasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:vendor');
    }

    public function changePasswordForm()
    {
        $id = Auth::guard('vendor')->id();
        $user = Vendor::find($id);
        return view('vendor.auth.change-password', compact('user'));
    }

    public function changePassword(Request $request)
    {
        $id = Auth::guard('vendor')->id();

        $this->validate($request, [
            'current_password' => 'required',
            'password' => 'required|min:8|confirmed',

        ]);

        $user = Vendor::find($id);

        if (Hash::check($request->get('current_password'), $user->password)) {

            $user->password = Hash::make($request->password);
            $user->save();

            return redirect()->route('vendor.password.form')->with('success', 'Password changed successfully!');

        } else {

            return redirect()->back()->with('error', 'Current password is incorrect');
        }

        return redirect()->route('vendor.password.form')->with('success', 'Password changed successfully');
    }

}
